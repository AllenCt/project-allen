package app.demo.api;

import app.demo.api.web.user.BOSearchUserAJAXRequest;
import app.demo.api.web.user.BOSearchUserAJAXResponse;
import app.demo.api.web.user.BOUserAJAXView;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author Allen
 */
public interface UserAJAXWebService {
    @GET
    @Path("/ajax/user/:id")
    BOUserAJAXView get(@PathParam("id")Long id);

    @GET
    @Path("/ajax/user")
    BOSearchUserAJAXResponse search(BOSearchUserAJAXRequest request);
}
