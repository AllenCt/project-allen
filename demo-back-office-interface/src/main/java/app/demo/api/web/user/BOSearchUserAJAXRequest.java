package app.demo.api.web.user;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author Allen
 */
public class BOSearchUserAJAXRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip = 0;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit = 1000;

    @QueryParam(name = "user_name")
    public String userName;

    @QueryParam(name = "pwd")
    public String pwd;

    @QueryParam(name = "sex")
    public Integer sex;

    @QueryParam(name = "age")
    public Integer age;
}
