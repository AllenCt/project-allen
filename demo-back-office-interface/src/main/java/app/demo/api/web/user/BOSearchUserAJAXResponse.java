package app.demo.api.web.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.util.List;

/**
 * @author Allen
 */
public class BOSearchUserAJAXResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "users")
    public List<User> users;

    public static class User {
        @NotNull
        @Property(name = "user_name")
        public String userName;

        @NotNull
        @Property(name = "pwd")
        public String pwd;

        @Property(name = "sex")
        public Integer sex;

        @Property(name = "age")
        public Integer age;
    }
}
