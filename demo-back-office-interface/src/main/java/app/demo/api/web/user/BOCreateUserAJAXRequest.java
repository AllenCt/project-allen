package app.demo.api.web.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author Allen
 */
public class BOCreateUserAJAXRequest {
    @NotNull
    @Property(name = "user_name")
    public String userName;

    @NotNull
    @Property(name = "pwd")
    public String pwd;

    @Property(name = "sex")
    public Integer sex;

    @Property(name = "age")
    public Integer age;
}
