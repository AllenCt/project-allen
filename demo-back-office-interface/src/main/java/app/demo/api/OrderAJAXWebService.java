package app.demo.api;

import app.demo.api.web.order.BOOrderAJAXView;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author Allen
 */
public interface OrderAJAXWebService {
    @GET
    @Path("/ajax/order/:id")
    BOOrderAJAXView get(@PathParam("id") String id);
}
