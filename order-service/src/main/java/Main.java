import app.OrderServiceApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new OrderServiceApp().start();
    }
}
