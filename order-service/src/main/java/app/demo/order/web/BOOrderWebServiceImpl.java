package app.demo.order.web;

import app.demo.api.BOOrderWebService;
import app.demo.api.web.order.BOOrderView;
import app.demo.order.service.BOOrderService;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class BOOrderWebServiceImpl implements BOOrderWebService {
    @Inject
    private BOOrderService boOrderService;
    @Override
    public BOOrderView get(String id) {
        return boOrderService.get(id);
    }
}
