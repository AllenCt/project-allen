package app.demo.order.web;

import app.demo.api.OrderWebService;
import app.demo.api.web.order.CreateOrderRequest;
import app.demo.api.web.order.OrderView;
import app.demo.api.web.order.SearchOrderRequest;
import app.demo.api.web.order.SearchOrderResponse;
import app.demo.order.service.OrderService;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class OrderWebServiceImpl implements OrderWebService {
    @Inject
    OrderService orderService;
    @Override
    public OrderView create(CreateOrderRequest request) {
        return orderService.create(request);
    }

    @Override
    public SearchOrderResponse search(SearchOrderRequest request) {
        return orderService.search(request);
    }
}
