package app.demo.order.domain;

import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

/**
 * @author Allen
 */
@Table(name = "orders")
public class Order {
    @PrimaryKey
    @Column(name = "order_id")
    public String orderId;

    @NotNull
    @Column(name = "order_name")
    public String orderName;

    @NotNull
    @Column(name = "address")
    public String address;

    @NotNull
    @Column(name = "price")
    public Double price;
}
