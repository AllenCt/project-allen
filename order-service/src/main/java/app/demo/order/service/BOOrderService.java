package app.demo.order.service;

import app.demo.api.web.order.BOOrderView;
import app.demo.order.domain.Order;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.NotFoundException;

/**
 * @author Allen
 */
public class BOOrderService {
    @Inject
    private Repository<Order> orderRepository;

    public BOOrderView get(String id) {
        Order order = orderRepository.get(id).orElseThrow(() -> new NotFoundException("order not found id " + id));
        return view(order);
    }

    private BOOrderView view(Order order) {
        BOOrderView orderView = new BOOrderView();
        orderView.orderId = order.orderId;
        orderView.orderName = order.orderName;
        orderView.address = order.address;
        orderView.price = order.price;
        return orderView;
    }
}
