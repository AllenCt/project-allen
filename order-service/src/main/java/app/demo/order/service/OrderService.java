package app.demo.order.service;

import app.demo.api.web.order.CreateOrderRequest;
import app.demo.api.web.order.OrderView;
import app.demo.api.web.order.SearchOrderRequest;
import app.demo.api.web.order.SearchOrderResponse;
import app.demo.order.domain.Order;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;

import java.util.stream.Collectors;

/**
 * @author Allen
 */
public class OrderService {
    @Inject
    private Repository<Order> orderRepository;

    public OrderView create(CreateOrderRequest request) {
        Order order = new Order();
        order.orderId = request.orderId;
        order.orderName = request.orderName;
        order.address = request.address;
        order.price = request.price;
        orderRepository.insert(order);
        return view(order);
    }

    public SearchOrderResponse search(SearchOrderRequest request) {
        SearchOrderResponse response = new SearchOrderResponse();
        Query<Order> query = orderRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        query.where("order_id = ? or order_name = ? or address = ? or price = ?", request.orderId, request.orderName, request.address, request.price);
        response.orders = query.fetch().stream().map(this::view).collect(Collectors.toList());
        response.total = query.count();
        return response;
    }
    
    private OrderView view(Order order) {
        OrderView orderView = new OrderView();
        orderView.orderId = order.orderId;
        orderView.orderName = order.orderName;
        orderView.address = order.address;
        orderView.price = order.price;
        return orderView;
    }
}
