package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author Allen
 */
public class OrderServiceApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        http().httpPort(8082);
        load(new OrderModule());
    }
}
