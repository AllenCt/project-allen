package app.web;

import app.demo.api.OrderAJAXWebService;
import app.demo.api.OrderWebService;
import app.demo.api.web.order.CreateOrderAJAXRequest;
import app.demo.api.web.order.CreateOrderRequest;
import app.demo.api.web.order.OrderAJAXView;
import app.demo.api.web.order.OrderView;
import app.demo.api.web.order.SearchOrderAJAXRequest;
import app.demo.api.web.order.SearchOrderAJAXResponse;
import app.demo.api.web.order.SearchOrderRequest;
import app.demo.api.web.order.SearchOrderResponse;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

import java.util.ArrayList;

/**
 * @author Allen
 */
public class OrderAJAXWebServiceImpl implements OrderAJAXWebService {
    @Inject
    private OrderWebService orderWebService;

    @Override
    public OrderAJAXView create(CreateOrderAJAXRequest request) {
        CreateOrderRequest createOrderRequest = new CreateOrderRequest();
        createOrderRequest.orderId = request.orderId;
        createOrderRequest.orderName = request.orderName;
        createOrderRequest.address = request.address;
        createOrderRequest.price = request.price;
        ActionLogContext.put("create_order", request.orderId);
        return view(orderWebService.create(createOrderRequest));
    }

    @Override
    public SearchOrderAJAXResponse search(SearchOrderAJAXRequest request) {
        SearchOrderRequest searchOrderRequest = new SearchOrderRequest();
        searchOrderRequest.skip = request.skip;
        searchOrderRequest.limit = request.limit;
        searchOrderRequest.orderId = request.orderId;
        searchOrderRequest.orderName = request.orderName;
        searchOrderRequest.address = request.address;
        searchOrderRequest.price = request.price;
        SearchOrderResponse search = orderWebService.search(searchOrderRequest);

        SearchOrderAJAXResponse searchOrderAJAXResponse = new SearchOrderAJAXResponse();
        searchOrderAJAXResponse.orders = new ArrayList<>();

        for (OrderView order : search.orders) {
            SearchOrderAJAXResponse.Order order1 = new SearchOrderAJAXResponse.Order();
            order1.orderId = order.orderId;
            order1.orderName = order.orderName;
            order1.address = order.address;
            order1.price = order.price;
            searchOrderAJAXResponse.orders.add(order1);
        }
        searchOrderAJAXResponse.total = search.total;
        return searchOrderAJAXResponse;
    }

    private OrderAJAXView view(OrderView orderView) {
        OrderAJAXView orderAJAXView = new OrderAJAXView();
        orderAJAXView.orderId = orderView.orderId;
        orderAJAXView.orderName = orderView.orderName;
        orderAJAXView.address = orderView.address;
        orderAJAXView.price = orderView.price;
        return orderAJAXView;
    }
}
