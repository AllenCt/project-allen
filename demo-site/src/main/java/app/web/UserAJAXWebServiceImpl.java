package app.web;

import app.demo.api.CustomerWebService;
import app.demo.api.UserAJAXWebService;
import app.demo.api.customer.CreateUserRequest;
import app.demo.api.customer.SearchUserRequest;
import app.demo.api.customer.SearchUserResponse;
import app.demo.api.customer.UserView;
import app.demo.api.web.user.CreateUserAJAXRequest;
import app.demo.api.web.user.SearchUserAJAXRequest;
import app.demo.api.web.user.SearchUserAJAXResponse;
import app.demo.api.web.user.UserAJAXView;
import core.framework.inject.Inject;

import java.util.ArrayList;

/**
 * @author Allen
 */
public class UserAJAXWebServiceImpl implements UserAJAXWebService {
    @Inject
    private CustomerWebService customerWebService;

    @Override
    public SearchUserAJAXResponse login(SearchUserAJAXRequest request) {
        SearchUserRequest searchUserRequest = new SearchUserRequest();
        searchUserRequest.skip = request.skip;
        searchUserRequest.limit = request.limit;
        searchUserRequest.userName = request.userName;
        searchUserRequest.pwd = request.pwd;
        searchUserRequest.age = request.age;
        searchUserRequest.sex = request.sex;
        SearchUserResponse userResponse = customerWebService.login(searchUserRequest);
        SearchUserAJAXResponse searchUserAJAXResponse = new SearchUserAJAXResponse();
        searchUserAJAXResponse.users = new ArrayList<>();

        for (UserView user : userResponse.users) {
            SearchUserAJAXResponse.User user1 = new SearchUserAJAXResponse.User();
            user1.userName = user.userName;
            user1.pwd = user.pwd;
            user1.age = user.age;
            user1.sex = user.sex;
            searchUserAJAXResponse.users.add(user1);
        }

        searchUserAJAXResponse.total = userResponse.total;
        return searchUserAJAXResponse;
    }

    @Override
    public UserAJAXView register(CreateUserAJAXRequest request) {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.userName = request.userName;
        createUserRequest.pwd = request.pwd;
        createUserRequest.age = request.age;
        createUserRequest.sex = request.sex;
        return view(customerWebService.register(createUserRequest));
    }

    private UserAJAXView view(UserView userView) {
        UserAJAXView userAJAXView = new UserAJAXView();
        userAJAXView.userName = userView.userName;
        userAJAXView.pwd = userView.pwd;
        userAJAXView.age = userView.age;
        userAJAXView.sex = userView.sex;
        return userAJAXView;
    }
}
