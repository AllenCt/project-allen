package app;


import app.demo.api.CustomerWebService;
import app.demo.api.OrderAJAXWebService;
import app.demo.api.OrderWebService;
import app.demo.api.UserAJAXWebService;
import app.web.OrderAJAXWebServiceImpl;
import app.web.UserAJAXWebServiceImpl;
import core.framework.module.Module;

/**
 * @author neo
 */
public class WebModule extends Module {
    @Override
    protected void initialize() {
        api().client(CustomerWebService.class, requiredProperty("app.UserWebServiceUrl"));
        api().client(OrderWebService.class, requiredProperty("app.OrderWebServiceUrl"));

        api().service(UserAJAXWebService.class, bind(UserAJAXWebServiceImpl.class));
        api().service(OrderAJAXWebService.class, bind(OrderAJAXWebServiceImpl.class));
    }

}
