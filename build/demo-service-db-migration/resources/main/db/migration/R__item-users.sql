CREATE TABLE IF NOT EXISTS `users`(
  `id`    INT AUTO_INCREMENT,
  `user_name`  VARCHAR(50) NOT NULL,
  `pwd`     VARCHAR(50)  NOT NULL,
  `sex`  INT(2) NULL,
  `age`  INT(3) NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;
