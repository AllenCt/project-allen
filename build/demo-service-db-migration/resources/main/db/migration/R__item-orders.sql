CREATE TABLE IF NOT EXISTS `orders`(
  `order_id`    VARCHAR(30) NOT NULL,
  `order_name`  VARCHAR(50) NOT NULL,
  `address`     VARCHAR(50) NOT NULL,
  `price`   DOUBLE NOT NULL,
  PRIMARY KEY (`order_id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;
