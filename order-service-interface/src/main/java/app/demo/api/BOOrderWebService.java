package app.demo.api;

import app.demo.api.web.order.BOOrderView;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author Allen
 */
public interface BOOrderWebService {
    @GET
    @Path("/bo/order/:id")
    BOOrderView get(@PathParam("id") String id);
}
