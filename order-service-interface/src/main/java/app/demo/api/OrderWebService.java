package app.demo.api;

import app.demo.api.web.order.CreateOrderRequest;
import app.demo.api.web.order.OrderView;
import app.demo.api.web.order.SearchOrderRequest;
import app.demo.api.web.order.SearchOrderResponse;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author Allen
 */
public interface OrderWebService {
    @POST
    @Path("/order")
    @ResponseStatus(HTTPStatus.CREATED)
    OrderView create(CreateOrderRequest request);

    @GET
    @Path("/order")
    SearchOrderResponse search(SearchOrderRequest request);
}
