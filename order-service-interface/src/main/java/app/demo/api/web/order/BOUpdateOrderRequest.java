package app.demo.api.web.order;

import core.framework.api.json.Property;

/**
 * @author Allen
 */
public class BOUpdateOrderRequest {
    @Property(name = "order_id")
    public String orderId;

    @Property(name = "order_name")
    public String orderName;

    @Property(name = "address")
    public String address;

    @Property(name = "price")
    public Double price;
}
