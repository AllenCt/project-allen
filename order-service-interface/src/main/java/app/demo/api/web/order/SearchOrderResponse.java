package app.demo.api.web.order;

import core.framework.api.json.Property;

import java.util.List;

/**
 * @author Allen
 */
public class SearchOrderResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "orders")
    public List<OrderView> orders;
}
