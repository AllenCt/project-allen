package app.demo.api.web.order;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.util.List;

/**
 * @author Allen
 */
public class BOSearchOrderResponse {
    @Property(name = "total")
    public Long total;

    @Property(name = "orders")
    public List<Order> orders;

    public static class Order {
        @NotNull
        @Property(name = "order_id")
        public String orderId;

        @NotNull
        @Property(name = "order_name")
        public String orderName;

        @NotNull
        @Property(name = "address")
        public String address;

        @NotNull
        @Property(name = "price")
        public Double price;
    }
}
