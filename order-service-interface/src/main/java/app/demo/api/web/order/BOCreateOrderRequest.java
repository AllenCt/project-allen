package app.demo.api.web.order;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author Allen
 */
public class BOCreateOrderRequest {
    @NotNull
    @Property(name = "order_id")
    public String orderId;

    @NotNull
    @Property(name = "order_name")
    public String orderName;

    @NotNull
    @Property(name = "address")
    public String address;

    @NotNull
    @Property(name = "price")
    public Double price;
}
