package app.demo.api;

import app.demo.api.web.order.CreateOrderAJAXRequest;
import app.demo.api.web.order.OrderAJAXView;
import app.demo.api.web.order.SearchOrderAJAXRequest;
import app.demo.api.web.order.SearchOrderAJAXResponse;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author Allen
 */
public interface OrderAJAXWebService {
    @POST
    @Path("/ajax/order")
    @ResponseStatus(HTTPStatus.CREATED)
    OrderAJAXView create(CreateOrderAJAXRequest request);

    @GET
    @Path("/ajax/order")
    SearchOrderAJAXResponse search(SearchOrderAJAXRequest request);
}
