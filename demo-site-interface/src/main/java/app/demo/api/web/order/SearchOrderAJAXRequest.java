package app.demo.api.web.order;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author Allen
 */
public class SearchOrderAJAXRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip = 0;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit = 1000;

    @QueryParam(name = "order_id")
    public String orderId;

    @QueryParam(name = "order_name")
    public String orderName;

    @QueryParam(name = "address")
    public String address;

    @QueryParam(name = "price")
    public Double price;
}
