package app.demo.api;

import app.demo.api.web.user.CreateUserAJAXRequest;
import app.demo.api.web.user.SearchUserAJAXRequest;
import app.demo.api.web.user.SearchUserAJAXResponse;
import app.demo.api.web.user.UserAJAXView;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author Allen
 */
public interface UserAJAXWebService {
    @GET
    @Path("/ajax/login")
    SearchUserAJAXResponse login(SearchUserAJAXRequest request);

    @POST
    @Path("/ajax/register")
    @ResponseStatus(HTTPStatus.CREATED)
    UserAJAXView register(CreateUserAJAXRequest request);
}
