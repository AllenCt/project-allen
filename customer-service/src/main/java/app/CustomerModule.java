package app;

import app.demo.api.BOCustomerWebService;
import app.demo.api.CustomerWebService;
import app.demo.customer.domain.User;
import app.demo.customer.service.BOUserService;
import app.demo.customer.service.UserService;
import app.demo.customer.web.BOUserWebServiceImpl;
import app.demo.customer.web.UserWebServiceImpl;
import core.framework.module.Module;

/**
 * @author Allen
 */
public class CustomerModule extends Module {
    @Override
    protected void initialize() {
        db().repository(User.class);
        bind(UserService.class);
        bind(BOUserService.class);
        api().service(CustomerWebService.class, bind(UserWebServiceImpl.class));
        api().service(BOCustomerWebService.class, bind(BOUserWebServiceImpl.class));
    }
}
