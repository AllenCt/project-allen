package app.demo.customer.service;

import app.demo.api.customer.BOSearchUserRequest;
import app.demo.api.customer.BOSearchUserResponse;
import app.demo.api.customer.BOUserView;
import app.demo.customer.domain.User;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.NotFoundException;

import java.util.stream.Collectors;

/**
 * @author Allen
 */
public class BOUserService {
    @Inject
    private Repository<User> userRepository;

    public BOUserView get(Long id) {
        User user = userRepository.get(id).orElseThrow(() -> new NotFoundException("user not found,id" + id));
        return view(user);
    }

    public BOSearchUserResponse search(BOSearchUserRequest request) {
        BOSearchUserResponse response = new BOSearchUserResponse();
        Query<User> query = userRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        query.where("user_name = ? or pwd = ? or sex = ? or age = ?", request.userName, request.pwd, request.sex, request.age);
        response.users = query.fetch().stream().map(this::searchView).collect(Collectors.toList());
        response.total = query.count();
        return response;
    }

    private BOSearchUserResponse.User searchView(User userView) {
        BOSearchUserResponse.User user = new BOSearchUserResponse.User();
        user.userName = userView.userName;
        user.pwd = userView.pwd;
        user.age = userView.age;
        user.sex = userView.sex;
        return user;
    }

    private BOUserView view(User user) {
        BOUserView boUserView = new BOUserView();
        boUserView.userName = user.userName;
        boUserView.pwd = user.pwd;
        boUserView.age = user.age;
        boUserView.sex = user.sex;
        return boUserView;
    }
}
