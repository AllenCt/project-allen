package app.demo.customer.service;

import app.demo.api.customer.CreateUserRequest;
import app.demo.api.customer.SearchUserRequest;
import app.demo.api.customer.SearchUserResponse;
import app.demo.api.customer.UserView;
import app.demo.customer.domain.User;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;

import java.util.stream.Collectors;

/**
 * @author Allen
 */
public class UserService {
    @Inject
    private Repository<User> userRepository;

    public SearchUserResponse login(SearchUserRequest request) {
        SearchUserResponse response = new SearchUserResponse();
        Query<User> query = userRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.userName) && !Strings.isBlank(request.pwd)) {
            query.where("user_name = ? and pwd = ?", request.userName, request.pwd);
        }
        response.users = query.fetch().stream().map(this::view).collect(Collectors.toList());
        response.total = query.count();
        return response;
    }

    public UserView register(CreateUserRequest request) {
        User user = new User();
        user.userName = request.userName;
        user.pwd = request.pwd;
        if (request.sex != null) {
            user.sex = request.sex;
        }
        if (request.age != null) {
            user.age = request.age;
        }
        userRepository.insert(user);
        return view(user);
    }

    private UserView view(User user) {
        UserView userView = new UserView();
        userView.userName = user.userName;
        userView.pwd = user.pwd;
        userView.sex = user.sex;
        userView.age = user.age;
        return userView;
    }
}
