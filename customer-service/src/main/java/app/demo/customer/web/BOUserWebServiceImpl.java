package app.demo.customer.web;

import app.demo.api.BOCustomerWebService;
import app.demo.api.customer.BOSearchUserRequest;
import app.demo.api.customer.BOSearchUserResponse;
import app.demo.api.customer.BOUserView;
import app.demo.customer.service.BOUserService;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class BOUserWebServiceImpl implements BOCustomerWebService {
    @Inject
    private BOUserService boUserService;

    @Override
    public BOUserView get(Long id) {
        return boUserService.get(id);
    }

    @Override
    public BOSearchUserResponse search(BOSearchUserRequest request) {
        return boUserService.search(request);
    }

}
