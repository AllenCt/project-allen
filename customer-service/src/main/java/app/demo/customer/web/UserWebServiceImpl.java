package app.demo.customer.web;

import app.demo.api.CustomerWebService;
import app.demo.api.customer.CreateUserRequest;
import app.demo.api.customer.SearchUserRequest;
import app.demo.api.customer.SearchUserResponse;
import app.demo.api.customer.UserView;
import app.demo.customer.service.UserService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author Allen
 */
public class UserWebServiceImpl implements CustomerWebService {
    @Inject
    private UserService userService;


    @Override
    public SearchUserResponse login(SearchUserRequest request) {
        return userService.login(request);
    }

    @Override
    public UserView register(CreateUserRequest request) {
        ActionLogContext.put("create_user", request.userName);
        return userService.register(request);
    }
}
