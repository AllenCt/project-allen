package app.demo.customer.domain;

import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

/**
 * @author Allen
 */
@Table(name = "users")
public class User {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "id")
    public Long id;

    @NotNull
    @Column(name = "user_name")
    public String userName;

    @NotNull
    @Column(name = "pwd")
    public String pwd;

    @Column(name = "sex")
    public Integer sex;

    @Column(name = "age")
    public Integer age;
}
