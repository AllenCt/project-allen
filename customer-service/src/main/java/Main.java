import app.CustomerServiceApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new CustomerServiceApp().start();
    }
}
