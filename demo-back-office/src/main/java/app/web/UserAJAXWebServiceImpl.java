package app.web;

import app.demo.api.BOCustomerWebService;
import app.demo.api.UserAJAXWebService;
import app.demo.api.customer.BOSearchUserRequest;
import app.demo.api.customer.BOSearchUserResponse;
import app.demo.api.customer.BOUserView;
import app.demo.api.web.user.BOSearchUserAJAXRequest;
import app.demo.api.web.user.BOSearchUserAJAXResponse;
import app.demo.api.web.user.BOUserAJAXView;
import core.framework.inject.Inject;

import java.util.ArrayList;

/**
 * @author Allen
 */
public class UserAJAXWebServiceImpl implements UserAJAXWebService {
    @Inject
    private BOCustomerWebService boCustomerWebService;

    @Override
    public BOUserAJAXView get(Long id) {
        return view(boCustomerWebService.get(id));
    }

    @Override
    public BOSearchUserAJAXResponse search(BOSearchUserAJAXRequest request) {
        BOSearchUserRequest searchUserRequest = new BOSearchUserRequest();
        searchUserRequest.skip = request.skip;
        searchUserRequest.limit = request.limit;
        searchUserRequest.userName = request.userName;
        searchUserRequest.pwd = request.pwd;
        searchUserRequest.age = request.age;
        searchUserRequest.sex = request.sex;
        BOSearchUserResponse userResponse = boCustomerWebService.search(searchUserRequest);
        BOSearchUserAJAXResponse searchUserAJAXResponse = new BOSearchUserAJAXResponse();
        searchUserAJAXResponse.users = new ArrayList<>();

        for (BOSearchUserResponse.User user : userResponse.users) {
            BOSearchUserAJAXResponse.User user1 = new BOSearchUserAJAXResponse.User();
            user1.userName = user.userName;
            user1.pwd = user.pwd;
            user1.age = user.age;
            user1.sex = user.sex;
            searchUserAJAXResponse.users.add(user1);
        }
        searchUserAJAXResponse.total = userResponse.total;
        return searchUserAJAXResponse;
    }

    private BOUserAJAXView view(BOUserView boUserView) {
        BOUserAJAXView boUserAJAXView = new BOUserAJAXView();
        boUserAJAXView.userName = boUserView.userName;
        boUserAJAXView.pwd = boUserView.pwd;
        boUserAJAXView.age = boUserView.age;
        boUserAJAXView.sex = boUserView.sex;
        return boUserAJAXView;
    }
}
