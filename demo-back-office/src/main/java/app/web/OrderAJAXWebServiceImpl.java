package app.web;

import app.demo.api.BOOrderWebService;
import app.demo.api.OrderAJAXWebService;
import app.demo.api.web.order.BOOrderAJAXView;
import app.demo.api.web.order.BOOrderView;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class OrderAJAXWebServiceImpl implements OrderAJAXWebService {
    @Inject
    private BOOrderWebService boOrderWebService;

    @Override
    public BOOrderAJAXView get(String id) {
        return view(boOrderWebService.get(id));
    }

    private BOOrderAJAXView view(BOOrderView orderView) {
        BOOrderAJAXView orderAJAXView = new BOOrderAJAXView();
        orderAJAXView.orderId = orderView.orderId;
        orderAJAXView.orderName = orderView.orderName;
        orderAJAXView.address = orderView.address;
        orderAJAXView.price = orderView.price;
        return orderAJAXView;
    }
}
