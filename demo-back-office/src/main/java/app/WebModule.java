package app;


import app.demo.api.BOCustomerWebService;
import app.demo.api.BOOrderWebService;
import app.demo.api.OrderAJAXWebService;
import app.demo.api.UserAJAXWebService;
import app.web.OrderAJAXWebServiceImpl;
import app.web.UserAJAXWebServiceImpl;
import core.framework.module.Module;

/**
 * @author neo
 */
public class WebModule extends Module {
    @Override
    protected void initialize() {
        api().client(BOCustomerWebService.class, requiredProperty("app.UserWebServiceUrl"));
        api().client(BOOrderWebService.class, requiredProperty("app.OrderWebServiceUrl"));

        api().service(UserAJAXWebService.class, bind(UserAJAXWebServiceImpl.class));
        api().service(OrderAJAXWebService.class, bind(OrderAJAXWebServiceImpl.class));
    }
}
