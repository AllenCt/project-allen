import app.DemoBackOfficeApp;

/**
 * @author neo
 */
public class Main {
    public static void main(String[] args) {
        new DemoBackOfficeApp().start();
    }
}
