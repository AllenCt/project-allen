package app.demo.api.customer;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author Allen
 */
public class BOSearchUserRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip = 0;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit = 1000;

    @QueryParam(name = "user_name")
    public String userName;

    @QueryParam(name = "pwd")
    public String pwd;

    @QueryParam(name = "sex")
    public Integer sex;

    @QueryParam(name = "age")
    public Integer age;
}
