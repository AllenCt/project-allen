package app.demo.api.customer;

import core.framework.api.json.Property;

/**
 * @author Allen
 */
public class UpdateUserRequest {
    @Property(name = "user_name")
    public String userName;

    @Property(name = "pwd")
    public String pwd;

    @Property(name = "sex")
    public Integer sex;

    @Property(name = "age")
    public Integer age;
}
