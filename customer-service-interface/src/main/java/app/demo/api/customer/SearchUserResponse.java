package app.demo.api.customer;

import core.framework.api.json.Property;

import java.util.List;

/**
 * @author Allen
 */
public class SearchUserResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "users")
    public List<UserView> users;
}
