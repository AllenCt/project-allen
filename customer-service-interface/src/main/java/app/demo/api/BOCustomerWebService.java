package app.demo.api;

import app.demo.api.customer.BOSearchUserRequest;
import app.demo.api.customer.BOSearchUserResponse;
import app.demo.api.customer.BOUserView;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author Allen
 */
public interface BOCustomerWebService {
    @GET
    @Path("/bo/user/:id")
    BOUserView get(@PathParam("id")Long id);

    @GET
    @Path("/bo/user")
    BOSearchUserResponse search(BOSearchUserRequest request);
}
