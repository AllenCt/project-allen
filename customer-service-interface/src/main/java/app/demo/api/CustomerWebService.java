package app.demo.api;

import app.demo.api.customer.CreateUserRequest;
import app.demo.api.customer.SearchUserRequest;
import app.demo.api.customer.SearchUserResponse;
import app.demo.api.customer.UserView;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author Allen
 */
public interface CustomerWebService {
    @GET
    @Path("/login")
    SearchUserResponse login(SearchUserRequest request);

    @POST
    @Path("/register")
    @ResponseStatus(HTTPStatus.CREATED)
    UserView register(CreateUserRequest request);
}
